import sys
import os

if __name__ == '__main__':
    input_source = os.environ.get('SOURCE').split()
    input_position = int(os.environ.get('POSITION', 0))
    input_destination = os.environ.get('DESTINATION').split()

    for i in range(len(input_source)):
        input_destination.insert(i + input_position, input_source[i])

    print(input_destination)
